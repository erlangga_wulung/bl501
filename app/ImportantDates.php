<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportantDates extends Model {

	//
	public function event()
	{
		return $this->belongsTo('App\Event', 'id_event');
	}

}
