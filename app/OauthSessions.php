<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthSessions extends Model {

	//
	public function token(){
		return $this->hasOne('App\OauthAccessToken');
	}

	public function user(){
		return $this->belongsTo('App\User', 'id');
	}

}