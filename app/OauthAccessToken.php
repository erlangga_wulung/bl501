<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model {

	//
	public function session(){
		return $this->hasOne('App\OauthSessions','id', 'session_id');
	}

	public function user(){
		return $this->hasManyThrough('App\OauthSessions', 'App\OauthAccessToken', 'id', 'session_id');
	}
}