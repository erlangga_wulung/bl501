<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	//
	public function user()
	{
		return $this->belongsTo('App\User', 'id_user');
	}

	public function subscribes()
	{	
		return $this->belongsToMany('App\User', 'subscribers', 'id_event', 'id_user');
	}

	public function ImportantDates()
	{
		return $this->hasMany('App\ImportantDates', 'id_event');
	}

	public function Speaker()
	{
		return $this->hasMany('App\Speaker', 'id_event');
	}

	public function papers()
	{	
		return $this->belongsToMany('App\User', 'papers', 'id_event', 'id_user');
	}
}
