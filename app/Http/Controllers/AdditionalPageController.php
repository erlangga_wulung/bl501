<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AdditionalPageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$additionalPage = \App\AdditionalPage::all();
    	return response()->json($additionalPage, 200, array('Content-Type' => 'application/json'));
	}

	public function showCallForPaper($id)
	{
		$additionalPage = \App\AdditionalPage::where('type','1')->where('id_event', $id)->orderBy('id','desc')->first();
    	return response()->json($additionalPage, 200, array('Content-Type' => 'application/json'));
	}

	public function showCallForParticipant($id)
	{
		$additionalPage = \App\AdditionalPage::where('type','2')->where('id_event', $id)->orderBy('id','desc')->first();
    	return response()->json($additionalPage, 200, array('Content-Type' => 'application/json'));
	}

	public function showCommitteeMember($id)
	{
		$additionalPage = \App\AdditionalPage::where('type','3')->where('id_event', $id)->orderBy('id','desc')->first();
    	return response()->json($additionalPage, 200, array('Content-Type' => 'application/json'));
	}

	public function showCustom($id)
	{
		$additionalPage = \App\AdditionalPage::where('type','4')->where('id_event', $id)->orderBy('id','desc')->first();
    	return response()->json($additionalPage, 200, array('Content-Type' => 'application/json'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$rules = array (
			'post_title'					=> 'required',
			'description'				=> 'required',
			'id_event'					=> 'required',
			'type'						=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$additionalPage = new \App\AdditionalPage();
			$additionalPage->id_event					= Input::get('id_event');
			$additionalPage->post_title 				= Input::get('post_title');
			$additionalPage->tab_title 					= Input::get('tab_title');
			$additionalPage->description 				= Input::get('description');
			$additionalPage->start_date 				= Input::get('start_date');
			$additionalPage->start_time 				= Input::get('start_time');
			$additionalPage->end_date 					= Input::get('end_date');
			$additionalPage->end_time	 				= Input::get('end_time');
			$additionalPage->type 		 				= Input::get('type');
			$additionalPage->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added new post!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$additionalPage = \App\additionalPage::find($id);
		$additionalPage->id_event					= Input::get('id_event');
		$additionalPage->post_title 				= Input::get('post_title');
		$additionalPage->tab_title 					= Input::get('tab_title');
		$additionalPage->description 				= Input::get('description');
		$additionalPage->start_date 				= Input::get('start_date');
		$additionalPage->start_time 				= Input::get('start_time');
		$additionalPage->end_date 					= Input::get('end_date');
		$additionalPage->end_time	 				= Input::get('end_time');
		$additionalPage->type 		 				= Input::get('type');
		$additionalPage->save();
		$respon = array (
			'status'	=> '1',
			'message'	=> 'Successfully updated page!'
			);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
