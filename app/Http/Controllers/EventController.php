<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$events = \App\Event::all();
    	return response()->json($events, 200, array('Content-Type' => 'application/json'));
	}

	public function getLastById($id)
	{
		$events = \App\Event::where('id_user', $id)->orderBy('id','desc')->first();
		return response()->json($events, 200, array('Content-Type' => 'application/json'));	
	}
	
	public function getLast()
	{
		$events = \App\Event::orderBy('id','desc')->first();
		return response()->json($events, 200, array('Content-Type' => 'application/json'));	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation
		$rules = array (
			'id_user'				=> 'required',
			'name'					=> 'required',
			'description'			=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$event = new \App\Event();
			$event->name 						= Input::get('name');
			$event->location 					= Input::get('location');
			$event->description 				= Input::get('description');
			$event->id_user 					= Input::get('id_user');
			$event->mime_image					= Input::get('mime_image');
			$event->image 						= Input::get('image');
			$event->start_date 					= Input::get('start_date');
			$event->start_time 					= Input::get('start_time');
			$event->end_date					= Input::get('end_date');
			$event->end_time 					= Input::get('end_time');
			$event->save();


			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully created event!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		//
		$event = \App\Event::find($id);

    	// show event
    	return response()->json($event, 200, array('Content-Type' => 'application/json')); 
	}
	
	public function showByTicket($id)
	{
		//
		$tickets = \App\Event::where('id_event', $id)->get();
    	// show event
    	return response()->json($tickets, 200, array('Content-Type' => 'application/json')); 
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// update
		$event = \App\Event::find($id);
		$event->name 						= Input::get('name');
		$event->location 					= Input::get('location');
		$event->description 				= Input::get('description');
		$event->id_user 					= Input::get('id_user');
		$event->mime_image					= Input::get('mime_image');
		$event->image 						= Input::get('image');
		$event->start_date 					= Input::get('start_daate');
		$event->start_time 					= Input::get('start_time');
		$event->end_date					= Input::get('end_date');
		$event->end_time 					= Input::get('end_time');
		$event->save();

		$respon = array (
			'status'	=> '1',
			'message'	=> 'Successfully updated event!'
			);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getByUser($id)
	{
		//
		$events = \App\Event::where('id_user', $id)->get();
		// show event
    	return response()->json($events, 200, array('Content-Type' => 'application/json'));
	}

}
