<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$tickets = \App\Ticket::all();
    	return response()->json($tickets, 200, array('Content-Type' => 'application/json'));
	}

	public function participantTickets($id)
	{
		$tickets = \App\Ticket::where('id_event', $id)->where('type','2')->get();
		return response()->json($tickets, 200, array('Content-Type' => 'application/json'));
	}

	public function participantTicketsByUser($id)
	{
		$tickets = \App\Ticket::where('id_user', $id)->where('type','2')->get();
		return response()->json($tickets, 200, array('Content-Type' => 'application/json'));
	}

	public function presenterTickets($id)
	{
		$tickets = \App\Ticket::where('id_event', $id)->where('type','1')->get();
		return response()->json($tickets, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation
		$rules = array (
			'title'					=> 'required',
			'price'					=> 'required',
			'description'				=> 'required',
			'id_event'				=> 'required',
			'type'					=> 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$ticket = new \App\Ticket();
			$ticket->title 						= Input::get('title');
			$ticket->price 						= preg_replace("/[^A-Za-z0-9\-]/","",(Input::get('price')));
			$ticket->description 				= Input::get('description');
			$ticket->id_event					= Input::get('id_event');
			$ticket->quantity					= Input::get('quantity');
			$ticket->type 						= Input::get('type');
			$ticket->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added ticket!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		//
		$ticket = \App\Ticket::find($id);
    	// show event
    	return response()->json($ticket, 200, array('Content-Type' => 'application/json'));
	}

	public function showByEvent($id)
	{
		//
		$tickets = \App\Ticket::where('id_event', $id)->get();
    	// show event
    	return response()->json($tickets, 200, array('Content-Type' => 'application/json')); 
	}

	public function getFirstByEvent($id)
	{
		$ticket = \App\Ticket::where('id_event', $id)->orderBy('id')->first();
		return response()->json($ticket, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$ticket = \App\Ticket::find($id);
		$ticket->title 						= Input::get('title');
		$ticket->price 						= preg_replace("/[^A-Za-z0-9\-]/","",(Input::get('price')));
		$ticket->description 				= Input::get('description');
		$ticket->id_event					= Input::get('id_event');
		$ticket->quantity					= Input::get('quantity');
		$ticket->type 						= Input::get('type');
		$ticket->save();
		$respon = array (
			'status'	=> '1',
			'message'	=> 'Successfully updated ticket!'
			);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

	public function decrement($id)
	{
		$ticket = \App\Ticket::find($id);
		$ticket->quantity					= Input::get('quantity');
		$ticket->save();
		$respon = array (
			'status'	=> '1',
			'message'	=> 'Successfully updated ticket!'
			);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
