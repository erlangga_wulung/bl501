<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$category = \App\Category::all();
    	return response()->json($category, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'category'					=> 'required',
			'description'				=> 'required',
			'id_event'					=> 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$category = new \App\Category();
			$category->id_event					= Input::get('id_event');
			$category->category 				= Input::get('category');
			$category->description 				= Input::get('description');
			$category->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added new category!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function showByEvent($id)
	{
		//
		$categories = \App\Category::where('id_event', $id)->get();
		return response()->json($categories, 200, array('Content-Type' => 'application/json'));
		
	}

	public function getFirstByEvent($id)
	{
		$categories = \App\Category::where('id_event', $id)->orderBy('id')->first();
		return response()->json($categories, 200, array('Content-Type' => 'application/json'));
	}

	public function show($id)
	{
		//
		$category = \App\Category::find($id);
		return response()->json($category, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$category = \App\Category::find($id);
		$category->id_event					= Input::get('id_event');
		$category->category 				= Input::get('category');
		$category->description 				= Input::get('description');
		$category->save();
		$respon = array (
			'status'	=> '1',
			'message'	=> 'Successfully updated category!'
			);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
