<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class SpeakerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$speakers = \App\Speaker::all();
    	return response()->json($speakers, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'name'					=> 'required',
			'institution'			=> 'required',
			'id_event'				=> 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$speaker = new \App\Speaker();
			$speaker->name 						= Input::get('name');
			$speaker->institution 				= Input::get('institution');
			$speaker->id_event 					= Input::get('id_event');
			$speaker->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added speaker!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$speaker = \App\Speaker::find($id);
    	// show event
    	return response()->json($speaker, 200, array('Content-Type' => 'application/json')); 
	}

	public function showByEvent($id)
	{
		//
		$speakers = \App\Speaker::where('id_event', $id)->get();
    	// show event
    	return response()->json($speakers, 200, array('Content-Type' => 'application/json')); 
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
