<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$tasks = \App\Task::all();
    	return response()->json($tasks, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		//
		// validate
		// read more on validation
		$rules = array (
			'title'					=> 'required',
			'expense'					=> 'required',
			'deadline'				=> 'required',
			'id_user'					=> 'required',
			'id_event'				=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$task = new \App\Task();
			$task->title 						= Input::get('title');
			$task->description 					= Input::get('description');
			$task->expense 						= preg_replace("/[^A-Za-z0-9\-]/","",(Input::get('expense')));
			$task->evidence_mime				= Input::get('evidence_mime');
			$task->evidence_name 				= Input::get('evidence_name');
			$task->evidence_blob 				= Input::get('evidence_blob');
			$task->deadline 					= Input::get('deadline');
			$task->id_user 						= Input::get('id_user');
			$task->id_event						= Input::get('id_event');
			$task->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added task!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$task = \App\Task::find($id);
    	// show event
    	return response()->json($task, 200, array('Content-Type' => 'application/json'));
	}

	public function showByEvent($id)
	{
		//
		$tasks = \App\Task::where('id_event', $id)->get();
    	// show event
    	return response()->json($tasks, 200, array('Content-Type' => 'application/json')); 
	}

	public function showByUser($id)
	{
		$tasks = \App\Task::where('id_user', $id)->get();
    	// show event
    	return response()->json($tasks, 200, array('Content-Type' => 'application/json')); 	
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
		// read more on validation
		$rules = array (
			'title'				=> 'required',
			'expense'			=> 'required',
			'status'			=> 'required',
			'deadline'			=> 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			// return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
			$respon = array (
				'status'	=> '0',
				'message'	=> 'Error updating user!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		} else {
			// store
			$task = \App\Task::find($id);
			$task->title 						= Input::get('title');
			$task->expense 						= preg_replace("/[^A-Za-z0-9\-]/","",(Input::get('expense')));
			$task->evidence_mime				= Input::get('evidence_mime');
			$task->evidence_name 				= Input::get('evidence_name');
			$task->evidence_blob 				= Input::get('evidence_blob');
			$task->comment 						= Input::get('comment');
			$task->status 						= Input::get('status');
			$task->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully updated task!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$task = \App\Task::find($id);
        $task->delete();
        $respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully deleted task!'
				);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

}
