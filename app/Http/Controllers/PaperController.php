<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class PaperController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$papers_raw = \App\Paper::all();
		
		foreach ($papers_raw as $key => $value) 
		{
			$papers[] =	array
			(
				'id'	=> $value->id,
				'id_event' => $value->id_event,
				'id_user' => $value->id_user,
				'title'	=> $value->title,
				'created_at' => $value->created_at,
				'updated_at' => $value->updated_at,
			);
		}
		return response()->json($papers, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'id_user'				=> 'required',
			'id_event'				=> 'required',
			'title'					=> 'required',
			'keywords'				=> 'required',
			'abstract'				=> 'required',
			'mime'					=> 'required',
			'file'					=> 'required',
			'id_ticket'				=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$paper = new \App\Paper();
			$paper->id_user 				= Input::get('id_user');
			$paper->id_event 				= Input::get('id_event');
			$paper->id_ticket				= Input::get('id_ticket');
			$paper->id_category				= Input::get('id_category');
			$paper->title 					= Input::get('title');
			$paper->keywords 				= Input::get('keywords');
			$paper->abstract 				= Input::get('abstract');
			$paper->mime 					= Input::get('mime');
			$paper->file 					= Input::get('file');
			$paper->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully uploaded paper!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	public function storeFolder()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'id_user'				=> 'required',
			'id_event'				=> 'required',
			'title'					=> 'required',
			'mime'					=> 'required',
			'filename'				=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$paper = new \App\PaperPath();
			$paper->id_user 				= Input::get('id_user');
			$paper->id_event 				= Input::get('id_event');
			$paper->title 					= Input::get('title');
			$paper->mime 					= Input::get('mime');
			$paper->file 					= Input::file('file');
			$paper->filename 					= Input::get('filename');
			$paper->pathinfo 				= Input::get('path');
			$paper->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully uploaded paper!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$paper = \App\Paper::find($id);
		return response()->json($paper, 200, array('Content-Type' => 'application/json'));
		
	}

	public function showByEvent($id)
	{
		//
		$event_papers_raw = \App\Paper::where('id_event', $id)->get();
		if(count($event_papers_raw) !== 0)
		{
			foreach ($event_papers_raw as $key => $value) 
			{
				$event_papers[] =	array
				(
					'id'	=> $value->id,
					'id_event' => $value->id_event,
					'id_user' => $value->id_user,
					'title'	=> $value->title,
					'created_at' => $value->created_at,
					'updated_at' => $value->updated_at,
				);
			}
			return response()->json($event_papers, 200, array('Content-Type' => 'application/json'));
		} else {
			return "[]";
		}
	}

	public function showByUser($id)
	{
		//
		$papers_raw = \App\Paper::where('id_user', $id)->get();
		if(count($papers_raw) !== 0)
		{
			foreach ($papers_raw as $key => $value) 
			{
				$papers[] =	array
				(
					'id'	=> $value->id,
					'id_event' => $value->id_event,
					'id_ticket' => $value->id_ticket,
					'title'	=> $value->title,
					'status' => $value->status,
					'hash' => $value->hash,
					'payment_status' => $value->payment_status,
					'created_at' => $value->created_at,
					'updated_at' => $value->updated_at,
				);
			}
			return response()->json($papers, 200, array('Content-Type' => 'application/json'));
		} else {
			return "[]";
		}
	}

	public function showOrderByUser($id)
	{
		$orders_raw = \App\Paper::where('id_user', $id)->get();
		// $orders_raw = \App\Paper::where('id_user', $id)->get();	
		if(count($orders_raw) !== 0)
		{
			foreach ($orders_raw as $key => $value) 
			{
				$orders[] =	array
				(
					'id'	=> $value->id,
					'id_event' => $value->id_event,
					'id_ticket' => $value->id_ticket,
					'title'	=> $value->title,
					'status' => $value->status,
					'payment_status' => $value->payment_status,
					'created_at' => $value->created_at,
					'updated_at' => $value->updated_at,
				);
			}
			return response()->json($orders, 200, array('Content-Type' => 'application/json'));
		}
	}

	public function showUserByTicket($id)
	{
		$orders_raw = \App\Paper::where('hash', $id)->get();
		// $orders_raw = \App\Paper::where('id_user', $id)->get();	
		if(count($orders_raw) !== 0)
		{
			$orders =	array
				(
					'id'	=> $orders_raw[0]->id,
					'id_user' => $orders_raw[0]->id_user,
					'id_event' => $orders_raw[0]->id_event,
					'id_ticket' => $orders_raw[0]->id_ticket,
					'title'	=> $orders_raw[0]->title,
					'status' => $orders_raw[0]->status,
					'payment_status' => $orders_raw[0]->payment_status,
					'created_at' => $orders_raw[0]->created_at,
					'updated_at' => $orders_raw[0]->updated_at,
				);
			return response()->json($orders, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

	}

	public function pay($id)
	{
		// validate
		// read more on validation
		$rules = array (
			'payment_status'			=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			// return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
			$respon = array (
				'status'	=> '0',
				'message'	=> 'Payment error!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		} else {
			// store
			$paper = \App\Paper::find($id);
			$paper->payment_status 				= Input::get('payment_status');
			$paper->hash = abs(crc32($id));
			$paper->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully paid!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$paper = \App\Paper::find($id);
        $paper->delete();
        $respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully deleted paper!'
				);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

}
