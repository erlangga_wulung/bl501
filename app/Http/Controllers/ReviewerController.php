<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ReviewerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$reviews = \App\Review::all();
		return response()->json($reviews, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'id_reviewer'				=> 'required',
			'id_paper'				=> 'required',
			'status'				=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$review = new \App\Review();
			$review->id_reviewer 				= Input::get('id_reviewer');
			$review->id_paper					= Input::get('id_paper');
			$review->status 					= Input::get('status');
			$review->comment 					= Input::get('comment');
			$review->rating 					= Input::get('rating');
			$review->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully assigned reviewer!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$review = \App\Review::find($id);
		return response()->json($review, 200, array('Content-Type' => 'application/json'));
	}

	public function showByUser($id)
	{
		$review = \App\Review::where('id_reviewer', $id)->get();
		return response()->json($review, 200, array('Content-Type' => 'application/json'));
	}

	public function showByPaper($id)
	{
		$review = \App\Review::where('id_paper', $id)->get();
		return response()->json($review, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'comment'				=> 'required',
			'status'			=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			// return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
			$respon = array (
				'status'	=> '0',
				'message'	=> 'Error updating user!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		} else {
			// store
			$review = \App\Review::find($id);
			$review->comment 				= Input::get('comment');
			$review->status 				= Input::get('status');
			$review->rating 				= Input::get('rating');
			$review->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully updated review!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$reviewer = \App\Review::find($id);
        $reviewer->delete();
        $respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully deleted paper review!'
				);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

}
