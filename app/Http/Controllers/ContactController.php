<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$contacts = \App\Contact::all();
    	return response()->json($contacts, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'name'					=> 'required',
			'email'					=> 'required',
			'address'				=> 'required',
			'phone'					=> 'required',
			'id_event'				=> 'required'			
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$contact = new \App\Contact();
			$contact->name 						= Input::get('name');
			$contact->email 					= Input::get('email');
			$contact->address 					= Input::get('address');
			$contact->phone 					= Input::get('phone');
			$contact->id_event					= Input::get('id_event');
			$contact->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added contact!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$contact = \App\Contact::find($id);
    	// show event
    	return response()->json($contact, 200, array('Content-Type' => 'application/json')); 
	}

	public function showByEvent($id)
	{
		//
		$contacts = \App\Contact::where('id_event', $id)->get();
    	// show event
    	return response()->json($contacts, 200, array('Content-Type' => 'application/json')); 
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
