<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//$users = User::all();
		$users = \App\User::all();
    	return response()->json($users, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation
		$rules = array (
			'name'			=> 'required',
			'email'			=> 'required|email',
			'password' 		=> 'required',
			'institution'	=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$user = new \App\User();
			$user->name 				= Input::get('name');
			$user->email 				= Input::get('email');
			$user->institution 			= Input::get('institution');
			$user->password 			= bcrypt(Input::get('password'));
			$user->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully created user!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$user = \App\User::find($id);

    	// show user
    	return response()->json($user, 200, array('Content-Type' => 'application/json')); 
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
		// read more on validation
		$rules = array (
			'name'			=> 'required',
			'email'			=> 'required|email',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			// return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
			$respon = array (
				'status'	=> '0',
				'message'	=> 'Error updating user!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		} else {
			// store
			$user = \App\User::find($id);
			$user->name 				= Input::get('name');
			$user->email 				= Input::get('email');
			// $user->password 			= bcrypt(Input::get('password'));
			$user->institution 			= Input::get('institution');
			$user->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully updated user!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	public function changePassword($id)
	{
		// read more on validation
		$rules = array (
			'password'			=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			// return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
			$respon = array (
				'status'	=> '0',
				'message'	=> 'Error updating user!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		} else {
			// store
			$user = \App\User::find($id);
			$user->password 			= bcrypt(Input::get('password'));
			$user->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully change password!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function getUserByToken($token){
		//
		$session = \App\OauthAccessToken::find($token);
		$respon = $session;
		// return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		if ($session !== NULL){
			$session_id = $session->session_id;
			$user_id = \App\OauthSessions::find($session_id)->owner_id;
			$user = \App\User::find($user_id);

			// show event
    		return response()->json($user, 200, array('Content-Type' => 'application/json'));	
		} else {
			$respon = array (
				'id'		=> 'xxx',
				'status'	=> '0',
				'message'	=> 'User not found!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}		
	}
	
	public function destroy($id)
	{
		//
		$user = \App\User::find($id);
        $user->delete();
        $respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully deleted user!'
				);
		return response()->json($respon, 200, array('Content-Type' => 'application/json'));
	}

}
