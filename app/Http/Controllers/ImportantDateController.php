<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ImportantDateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//
		$importantDates = \App\ImportantDates::all();
    	return response()->json($importantDates, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// validate
		// read more on validation
		$rules = array (
			'id_event'				=> 'required',
			'title'					=> 'required',
			'date'					=> 'required',
			'description'			=> 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$importantDate = new \App\ImportantDates();
			$importantDate->title						= Input::get('title');
			$importantDate->description 				= Input::get('description');
			$importantDate->date 						= Input::get('date');
			$importantDate->id_event 					= Input::get('id_event');
			$importantDate->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully added important date!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$importantDate = \App\ImportantDates::find($id);
    	// show event
    	return response()->json($importantDate, 200, array('Content-Type' => 'application/json')); 
	}

	public function showByEvent($id)
	{
		//
		$importantDates = \App\ImportantDates::where('id_event', $id)->get();
    	// show event
    	return response()->json($importantDates, 200, array('Content-Type' => 'application/json')); 
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
