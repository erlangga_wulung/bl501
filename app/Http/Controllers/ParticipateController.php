<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ParticipateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$participates = \App\Participate::all();
    	return response()->json($participates, 200, array('Content-Type' => 'application/json'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$rules = array (
			'id_user'					=> 'required',
			'id_ticket'					=> 'required',
			);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()){
			return response()->json($validator, 200, array('Content-Type' => 'application/javascript'));
		} else {
			// store
			//$user = new User;
			$participate = new \App\Participate();
			$participate->id_user 					= Input::get('id_user');
			$participate->id_ticket	 				= Input::get('id_ticket');
			$participate->save();

			$respon = array (
				'status'	=> '1',
				'message'	=> 'Successfully bought ticket!'
				);
			return response()->json($respon, 200, array('Content-Type' => 'application/json'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$participant = \App\Participate::find($id);
    	// show event
    	return response()->json($participant, 200, array('Content-Type' => 'application/json'));
	}

	public function participateByUser($id)
	{
		//
		$participant = \App\Participate::where('id_user', $id)->get();
    	// show event
    	return response()->json($participant, 200, array('Content-Type' => 'application/json'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
