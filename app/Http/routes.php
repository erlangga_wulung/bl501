<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::post('oauth/access_token', function() {
 return Response::json(Authorizer::issueAccessToken());
});


Route::get('api', ['before' => 'oauth', function() {
 // return the protected resource
 //echo “success authentication”;
 $user_id=Authorizer::getResourceOwnerId(); // the token user_id
 $user=\App\User::find($user_id);// get the user data from database

return Response::json($user);
}]);

// Route::get('/register ',function(){$user = new App\User();
//  $user->name="test user";
//  $user->email="test@test.com";
//  $user->password = \Illuminate\Support\Facades\Hash::make("password");
//  $user->save();
// });
Route::resource('users', 'UserController');
Route::put('change_password/{id}', 'UserController@changePassword');
Route::resource('events', 'EventController');
Route::get('get_last_event/{id}','EventController@getLastById');
Route::get('get_last_event','EventController@getLast');
Route::get('ticket_event/{id}','EventController@showByTicket');
Route::get('creator_event/{id}', 'EventController@getByUser');
Route::get('event_dates/{id}','ImportantDateController@showByEvent');
Route::resource('dates', 'ImportantDateController');
Route::get('users_token/{token}','UserController@getUserByToken');
Route::resource('speakers', 'SpeakerController');
Route::get('event_speakers/{id}','SpeakerController@showByEvent');
Route::resource('event_subscribers', 'SubscriberController');
Route::resource('contacts', 'ContactController');
Route::get('event_contacts/{id}','ContactController@showByEvent');
Route::resource('tasks','TaskController');
Route::get('event_tasks/{id}','TaskController@showByEvent');
Route::get('user_tasks/{id}','TaskController@showByUser');
Route::resource('tickets','TicketController');
Route::get('event_tickets/{id}','TicketController@showByEvent');
Route::get('event_ticket_first/{id}','TicketController@getFirstByEvent');
Route::get('participant_tickets/{id}','TicketController@participantTickets');
Route::get('participant_tickets_by_user/{id}','TicketController@participantTicketsByUser');
Route::get('presenter_tickets/{id}','TicketController@presenterTickets');
Route::put('tickets_decrement/{id}','TicketController@decrement');
Route::resource('papers','PaperController');
Route::get('event_papers/{id}','PaperController@showByEvent');
Route::get('user_papers/{id}','PaperController@showByUser');
Route::get('user_orders/{id}','PaperController@showOrderByUser');
Route::get('check_ticket/{id}','PaperController@showUserByTicket');
Route::put('pay/{id}','PaperController@pay');
Route::resource('reviews','ReviewerController');
Route::get('user_review/{id}','ReviewerController@showByUser');
Route::get('paper_review/{id}','ReviewerController@showByPaper');
Route::resource('categories','CategoryController');
Route::get('event_categories/{id}','CategoryController@showByEvent');
Route::get('event_categories_first/{id}','CategoryController@getFirstByEvent');
Route::resource('callForPaper','CallForPaperController');
Route::resource('callForParticipant','CallForParticipantController');
Route::resource('committeeMember','CommitteeMemberController');
Route::resource('additionalPage','AdditionalPageController');
Route::get('additionalPageCustom/{id}','AdditionalPageController@showCustom');
Route::get('additionalPageCallForPaper/{id}','AdditionalPageController@showCallForPaper');
Route::get('additionalPageCallForParticipant/{id}','AdditionalPageController@showCallForParticipant');
Route::get('additionalPageCommitteeMember/{id}','AdditionalPageController@showCommitteeMember');
Route::resource('participates','ParticipateController');
Route::get('participates_user/{id}','ParticipateController@participateByUser');